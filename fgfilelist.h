#ifndef _FGFILELIST_H
#define _FGFILELIST_H

// fgfilelist.h

// Internal call which is the starting point for
// child threads
void* ThreadStartPoint(void* pArg);

class FGString;

class FGFileList {
public:
  // Constructor, desctructor
  FGFileList();
  ~FGFileList();

  // Loads file list from config rc file
  void LoadConfig(const FGString& configFile);

  // The all import "grab them all" method :-)
  void GetAllFiles(void) const;

private:
  // Banned!
  FGFileList(const FGFileList& other);
  FGFileList& operator=(const FGFileList& other);

  // Internal helpers
  FGString GetLineString(void) const;

  // Using the Cheshire Cat to hide implementation details
  struct Internal_FGFileList;
  Internal_FGFileList* mpInternals;

  int mLine;

  // Counter of active child rule threads
  // (shared across all threads)
  static int msThreadsActive;

  static void* ThreadStartPoint(void* pArg);
};

#endif // _FGFILELIST_H
