#ifndef _FGCONI_H
#define _FGCONI_H

// fgconi.h
//
// FTPGrabConnectionInterface
//
// Interface class detailing requirements of each FTPGrab protocol
// we might be fetching from e.g. FTP, HTTP etc.

class FGString;
class FGDirListing;

class FGConnectionInterface {
public:
  // Need virtual destructor - we delete by the base class
  virtual ~FGConnectionInterface();

  // Connect method
  // INPUTS: Hostname of host to connect to
  // RETURNS: Nothing
  // THROWS: Various connect failed exceptions
  virtual void Connect(const FGString& host) = 0;

  // Change directory method
  // INPUTS: Directory name
  // RETURNS: Nothing
  // THROWS: Directory not found exception
  virtual void ChangeDir(const FGString& dir) = 0;

  // Get directory listing method
  // INPUTS: None
  // RETURNS: Vector of directory entries
  virtual FGDirListing GetDirListing(void) = 0;

  // Retrieve file method
  // INPUTS: Filename
  // RETURNS: fd to source of data
  // THROWS: File not found, etc.
  virtual int GetFile(const FGString& file) = 0;

  // Post retrive file method
  // Some protocols need to perform cleanup/verification after
  // a transfer
  virtual void PostFileTransfer(void) = 0;

  // Disconnect
  virtual void Disconnect(void) = 0;
};

#endif // _FGCONI_H
