#ifndef _FGFILEINFO_H
#define _FGFILEINFO_H

// fgfileinfo.h

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

class FGFileInfo {
public:
  // Construct from filename
  FGFileInfo(const FGString& filename);

  // Construct from filename, size, isDir, isFile
  FGFileInfo(const FGString& filename, int size, bool isDir, bool isFile);

  // Pesky STL needs this
  FGFileInfo();

  // Do two files have identical attributes?
  bool operator==(const FGFileInfo& other) const;

  // Get methods
  const FGString& GetFileName(void) const;
  bool IsRegularFile(void) const;
  int GetSize(void) const;

private:
  FGString mFileName;
  int mSize;
  bool mIsDir;
  bool mIsRegularFile;
  // TODO: Date modified
};

#endif // _FGFILEINFO_H
