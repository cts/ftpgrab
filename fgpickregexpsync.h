#ifndef _FGPICKREGEXPSYNC_H
#define _FGPICKREGEXPSYNC_H

// fgpickregexpsync.h
//
// Implementation of a file picker that selects all remote files matching
// a given regular expression for which we do not have remote copies.
// Also, all local files matching the regexp which are not on the remote
// site, are removed

#ifndef _FGFPICKI_H
#include "fgfpicki.h"
#endif

class FGConnectionInterface;
class FGString;
struct re_pattern_buffer;
typedef struct re_pattern_buffer regex_t;

class FGPickRegexpSync : public FGFilePickerInterface {
public:
  // Must be constructed with connection interface and regexp match string
  FGPickRegexpSync(FGConnectionInterface* pConnIf,
                   const FGString& regexp);
  virtual ~FGPickRegexpSync();

  virtual FGActionList DecideActions(const FGDirListing& localDir,
                                     const FGDirListing& remoteDir);

private:
  // Banned!
  FGPickRegexpSync(const FGPickRegexpSync& other);
  FGPickRegexpSync& operator=(const FGPickRegexpSync& other);

  // Internal representation of a regexp
  regex_t* mpRegExp;
};

#endif // _FGPICKREGEXP_H
