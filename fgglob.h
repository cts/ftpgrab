#ifndef _FGGLOB_H
#define _FGGLOB_H

class FGGlob {
public:
  static FGString gHostName;
  static bool gLogDisabled;

  // Max. num threads on the go at once
  static int gMaxThreads;

  // Are we outputting progress to stdout
  static bool gVerbose;
};

#endif // _FGGLOB_H


