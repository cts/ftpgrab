#ifndef _FGACTIONI_H
#define _FGACTIONI_H

// fgactioni.h
//
// FGActionInterface
//
// Interface to individual actions e.g. "get" "delete" etc.
// Very simple :-)

class FGConnectionInterface;

class FGActionInterface {
public:
  // Need virtual destructor for the mm
  virtual ~FGActionInterface();

  // Non-virtual method which basically delegates to VirtualDo() but
  // handles exceptions by automatically calling abort and re-throwing
  void Do(void) const;

  virtual void VirtualDo(void) const = 0;

  // Abort: called if operation exits abnormally
  // Example: we are downloading a file when we lose the connection. In
  // this case we call Abort() and the specific cleanup is to delete the
  // partially downloaded file
  virtual void Abort(void) const = 0;
};

#endif // _FGACTIONI_H
