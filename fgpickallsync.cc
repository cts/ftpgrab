// fgpickallsync.cc

#include "fgpickallsync.h"

// Dir listings
#ifndef _FGDLIST_H
#include "fgdlist.h"
#endif

// Action lists
#ifndef _FGALIST_H
#include "fgalist.h"
#endif

// The download file action
#ifndef _FGDLACTION_H
#include "fgdlaction.h"
#endif

// The local erase file action
#ifndef _FGDELACTION_H
#include "fgdelaction.h"
#endif

FGPickAllSync::FGPickAllSync(FGConnectionInterface* pConnIf)
: FGFilePickerInterface(pConnIf)
{
}

FGActionList
FGPickAllSync::DecideActions(const FGDirListing& localDir,
                             const FGDirListing& remoteDir)
{
  // The picker for the "normal" wildcard operation (non recursive),
  // leaves all local files intact
  // Very simple rule: grab any file in the remoteDir that is
  // not in the localDir
  FGActionList ret;

  // Get list of files in local but not remote
  FGDirListing staleFiles = FGDirListing::GetRHSOnlyFiles(remoteDir, localDir);

  // For each stale file, make an "erase" action
  std::vector<FGFileInfo>::const_iterator iFiles;
  for (iFiles = staleFiles.begin(); iFiles != staleFiles.end(); iFiles++) {
    FGActionInterface* pNewAction;

    pNewAction = new FGDeleteAction(iFiles->GetFileName(),
                                    localDir.GetDirName());
    ret.push_back(pNewAction);
  }

  // Get list of files in remote but not local
  FGDirListing newFiles = FGDirListing::GetRHSOnlyFiles(localDir, remoteDir);

  // Make a "download" action for all the above files
  for (iFiles = newFiles.begin(); iFiles != newFiles.end(); iFiles++) {
    FGActionInterface* pNewAction;

    pNewAction = new FGDownloadAction(iFiles->GetFileName(), mpConnIf,
                                      localDir.GetDirName(),
                                      iFiles->GetSize());
    ret.push_back(pNewAction);
  }

  return ret;
}
