#ifndef _FGPICKALL_H
#define _FGPICKALL_H

// fgpickall.h
//
// Implementation of a file picker that selects all remote files for
// which we do not have local copies

#ifndef _FGFPICKI_H
#include "fgfpicki.h"
#endif

class FGConnectionInterface;

class FGPickAll : public FGFilePickerInterface {
public:
  // Must be constructed with connection interface
  FGPickAll(FGConnectionInterface* pConnIf);

  virtual FGActionList DecideActions(const FGDirListing& localDir,
                                     const FGDirListing& remoteDir);

private:
  // Banned!
  FGPickAll(const FGPickAll& other);
  FGPickAll& operator=(const FGPickAll& other);
};

#endif // _FGPICKALL_H
