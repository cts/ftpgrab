#ifndef _FGFPICKI_H
#define _FGFPICKI_H

// fgfpicki.h
//
// FGFilePickerInterface
//
// Defines interface to deciding what files we want to download from
// remote and erase locally. We decide based upon a directory listing
// of the remote directory and a directory listing of the local
// directory.

class FGActionList;
class FGDirListing;
class FGConnectionInterface;

class FGFilePickerInterface {
public:
  // Construct taking connection on which to do downloads
  FGFilePickerInterface(FGConnectionInterface* pConnIf);

  virtual FGActionList DecideActions(const FGDirListing& localDir,
                                     const FGDirListing& remoteDir) = 0;
protected:
  // Some actions must be performed in the context of a connection
  FGConnectionInterface* mpConnIf;
};

#endif // _FGFPICKI_H
