#ifndef _FGFILEGRAB_H
#define _FGFILEGRAB_H

// fgfilegrab.h
//
// Definition of in-memory structure describing a single "fetch" operation.
// A fetch operation may fetch one file, or many files if it is for example
// based on a wildcard rule

#ifndef _FGSTRING_H
#include "fgstring.h"
#endif

class FGConnectionInterface;
class FGFilePickerInterface;

class FGFileGrab {
public:
  // Constructors
  FGFileGrab(const FGString& ruleName,
             const FGString& host, const FGString& remDir,
             const FGString& localDir, const FGString& file);

  // The all important "go get it" method
  void GrabIt(void) const;

  // Getters
  const FGString& GetRuleName(void) const;

  void FreeResources(void);

private:
  // Implementation details
  FGString mRuleName;
  FGString mRemoteHost;
  FGString mRemoteDir;
  FGString mLocalDir;

  // Interface to the connection protocol this fetch uses
  FGConnectionInterface* mpConInterface;

  // Interface to the method of selecting files this fetch uses
  FGFilePickerInterface* mpPickInterface;
};

#endif // _FGFILEGRAB_H
